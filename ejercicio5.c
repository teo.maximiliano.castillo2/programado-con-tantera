/*
    Ejercicio 2

    Modificar el programa del ejercicio anterior para que ademas de informar el promedio indique:
    - Maximo
    - Minimo
    - Cantidad de notas aprobadas
    - Cantidad de desaprobadas
*/

#include <stdio.h>

    //Variaables

    int nota1;
    int nota2; 
    int nota3; 
    int nota4; 
    int nota5;
    int aprobadas = 0;
    int desaprobadas = 0;
    int maximo = 0;
    int minimo = 0;

    int main (){

    //Valortes de notas

    printf ("Ingrese la primera nota:\n");
    scanf ("%d",&nota1);
    //Valores maximos y minimos
    if(nota1>=6){
        maximo = nota1;
    }
    else {
        minimo = nota1;
    }
    printf ("Ingrese la segunda nota:\n");
    scanf ("%d",&nota2);
    //Valores maximos y minimos
    if(nota2>=maximo){
        maximo = nota2;
    }
    else {
        minimo = nota2;
    }
    printf ("Ingrese la tercera nota:\n");
    scanf ("%d",&nota3);
    //Valores maximos y minimos
    if(nota3>=maximo){
        maximo = nota3;
    }
    else {
        minimo = nota3;
    }
    printf ("Ingrese la cuarta nota:\n");
    scanf ("%d",&nota4);
    //Valores maximos y minimos
    if(nota4>=maximo){
        maximo = nota4;
    }
    else {
        minimo = nota4;
    }
    printf ("Ingrese la quinta nota:\n");
    scanf ("%d",&nota5);
    //Valores maximos y minimos
    if(nota5>=maximo){
        maximo = nota5;
    }
    else {
        minimo = nota5;
    }
    printf ("La nota maxima es: %d\n",maximo);
    printf ("La nota minima es: %d\n",minimo);
    if(nota1>=1&&nota1<=10 && nota2>=1&&nota2<=10 && nota3>=1&&nota3<=10 && nota4>=1&&nota4<=10 && nota5>=1&&nota5<=10)
    {
        //Valores estan en rango
        printf("el promedio es %d\n",(float) (nota1+nota2+nota3+nota4+nota5) / 5);
    }
    //Valores no estan en rango
    else {
        return(0);
    }
    //Cantidad de notas desaprobadas y aprobadas
    if (nota1>=6){ 
        aprobadas++;
    }
    if (nota2>=6){ 
        aprobadas++;
    }
    if (nota3>=6){ 
        aprobadas++;
    }
    if (nota4>=6){ 
        aprobadas++;
    }
    if (nota5>=6){ 
        aprobadas++;
    }
    printf ("Notas aprobadas: %d\n",aprobadas);
    printf ("Notas desaprobadas: %d\n",5-aprobadas);
    return(0);
}